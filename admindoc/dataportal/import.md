---
layout: page
title: Import Data
nav_order: 2
has_children: false
parent: DataPortal
grand_parent: Admin Documentation
permalink: /admindoc/dataportal/import

---

# DataPortal - Import Data

Note: Before proceeding the following steps (building *pdx.graph* DB) make sure there is no *pdx.graph* DB in *~/Documents/*

**Option 1: InteliJ**
- Menu: *Run -> Edit Configuration*, select *Loader* 
- In dialog box *Run/Debug Configurations* in part *Enviroment/Program arguments* enter <code>-loadALL --pdxfinder.root.dir=xxx</code> where xxx is local path to EBI data 
- Menu: *Run -> Run Loader*
- Wait for 2-3 hours (!)
- Then the graph DB is generated in folder *~/Documents/pdx.graph*

	
**Option 2: Terminal**
- Go to directory *dataportal-sourcecode*
- Run <code>mvn clean package -DskipTests</code>
- Run <code>java -jar indexer/target/indexer-1.0.0.jar load --group=EurOPDX --data-dir=xxx</code> where xxx is local path to EBI data 
- The graph DB is generated in folder *~/Documents/pdx.graph*


**Uploading data to the DataPortal server**
- Go to folder *~/Documents/pdx.graph*
- Run <code>tar -czvf ../graph.tgz *</code> to create archive with the data in the upper folder
- Copy the file to DataPortal server via <code>scp graph.tgz pdxuser@datahub.edirex.ics.muni.cz:dataportal-docker/neo4j-data/</code>	
(archive with the data muse be located just in folder *neo4j-data* and have just name *graph.tgz*)	
- Log in to Dataportal server, go to folder */home/pdxuser/dataportal-docker*
- Drop down the containers via <code>docker-compose down<code>
- Delete volume with old N4J DB via <code>docker volume rm dataportal-docker_neodata</code>
- Start up the containers via <code>docker-compose up -d</code>
