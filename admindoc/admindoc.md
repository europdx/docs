---
layout: default
title: Admin Documentation 
nav_order: 5
has_children: true
permalink: /admindoc/

---

# Admin documentation

In this section please find administrator documentation for DataPortal infrastructure.