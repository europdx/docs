---
layout: default
title: Proxy
nav_order: 6
has_children: false
parent: cBioOnDemand
grand_parent: Admin Documentation
permalink: /admindoc/cbiood/proxy/

---

# Proxy

This component is used as reverse proxy(based on Apache), which redirects https traffic from users to the running cBioOnDemand instances. Reverse proxy rules are managed by Flask REST API, which is part of this software packages.
 Whole proxy is running in Kubernetes as a deployment.

## GitLab repository

[https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/secure-routing](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/secure-routing)

## API Documentation
[https://app.swaggerhub.com/apis/xpivarc/dynamic-Apache2/1.0.0](https://app.swaggerhub.com/apis/xpivarc/dynamic-Apache2/1.0.0)

## Structure
We have three branches which are identical with deployment environment.

Folder 'Docker' holds Dockerfiles and required files for building images for Proxy. Subfolder contains python extension for dynamic configuration.

These images are stored in Gitlab image repository. Tag policy is set as \<branch\>-\<hash_of_commit\>. With this policy we can identifies where and when was application/image built.

## Deployment/Pipelines
Everything should be templates and jobs are only special configuration of templates.

- Stages (master/beta/dev):
  - Build (auto/auto/auto)- build image for Proxy ('Docker folder'). Used in Deploy stage
  - Deploy (manual/manual/manual)- grab image of Proxy from build stage and deploy it to Kubernetes

## !WARNING!
As for now, there is no cleaning job for this images. Also there needs to be settle up some "removing images" policy. !!!

Folder 'k8s' contains configuration for Kubernetes (deployment + service for apache + service for python extension)
