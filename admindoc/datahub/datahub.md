---
layout: default
title: DataHub
nav_order: 3
has_children: true
parent: Admin Documentation
permalink: /admindoc/datahub/

---

# DataHub

Environment for Data Portal management. All related repositories are stored [here](https://gitlab.ics.muni.cz/europdx/datahub) under GitLab Group.
#### Structure
- [DataHub docker](https://gitlab.ics.muni.cz/europdx/datahub/datahub-docker) which represents the main application transporting data within Data Portal applications. Data Hub API is Python Flask application, allowing user to retrieve data through multiple resources, which are described [here](https://app.swaggerhub.com/apis-docs/RadimPesa/datahub/v1#/).
- [EBI parser](https://gitlab.ics.muni.cz/europdx/datahub/datahub-docker) which parses pre-UPDOG files and transform them into MySQL data set.
- [Data release](https://gitlab.ics.muni.cz/europdx/datahub/data-release) is git LFS repository containing input data.



<!---
- Zakladni info k cemu slouzi
- Z jakych komponent se sklada, jake jsou k tomu kontejnery
- Jak se s tema kontejnerama pracuje docker-compose...
- Pres jake komunikacni kanaly / porty se spolu ty kontejnery bavi
- ...
-->
