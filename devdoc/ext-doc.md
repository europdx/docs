---
layout: page
title: External Documentation
nav_order: 1
has_children: false
parent: Developer Documentation
permalink: /devdoc/ext-doc

---

# External Documentation

## Links to forked software

[PDXFinder](https://github.com/PDXFinder/pdxfinder) (DataPortal)

[cBioPortal](https://github.com/cBioPortal/cbioportal)


## Links to documentation for the software used

[Docker](https://docs.docker.com/get-started)

[Terraform](https://www.terraform.io/intro/index.html)

[Ansible](https://docs.ansible.com/ansible/latest/user_guide/quickstart.html)

[Git](https://git-scm.com/docs/git)

[GitLab](https://docs.gitlab.com/ee/README.html)


