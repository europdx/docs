---
layout: page
title: Installation
nav_order: 1
has_children: false
parent: DataPortal-dev
grand_parent: Developer Documentation
permalink: /devdoc/dataportal/install

---

# DataPortal - installation

It is necessary to have a blank machine.

For more info about creating virtual machines using terraform, see [Admin Documentation > Infrastructure](/admindoc/infrastructure/)

Installation process is provided by a GitLab CI/CD which use ansible to do necessary actions and install required software.

[GitLab - DataPortal Group](https://gitlab.ics.muni.cz/europdx/dataportal)

[GitLab - dataportal-docker-edirex-ansible](https://gitlab.ics.muni.cz/europdx/dataportal/dataportal-docker-edirex-ansible)

This ansible script **clones the repository, sets permissions, makes sure dataportal directory exists, copies data for neo4j container, copies mellon files and ssl files and edits .env file in dataportal dir**.

Next step is to start dataportal.

[GitLab - dataportal-docker](https://gitlab.ics.muni.cz/europdx/dataportal/dataportal-docker)

For this action is used another CI/CD. It just runs ``git pull`` to get dataportal up to date and ``docker-compose pull`` and ``docker-compose up -d``. Similar CI/CD is used in [dataportal-sourcecode repo](https://gitlab.ics.muni.cz/europdx/dataportal/dataportal-sourcecode) to re-deploy dataportal when some of the tree branches is edited.
