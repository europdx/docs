---
layout: default
title: API
nav_order: 5
has_children: false
parent: cBioOnDemand
grand_parent: Admin Documentation
permalink: /admindoc/cbiood/api/

---

# API

Java application which serves REST API for cBioOnDemand. Using Kubernetes java client library to deploy cBioPortal instances to Kubernetes environment and managing cBioPortal lifecycle.
API is also deployed as Kubernetes deployment.


## GitLab repository:
[https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/api](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/api)

## cBioOnDemand instances description
Configuration for these instances are located in java [https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/api/tree/master/cbioondemandK8S/src/main/resources](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/api/tree/master/cbioondemandK8S/src/main/resources).

## API documentation
[https://app.swaggerhub.com/apis/xpivarc/cbio-k8s-api/1.0.0](https://app.swaggerhub.com/apis/xpivarc/cbio-k8s-api/1.0.0)

## Structure
We have three branches which are identical with deployment environment.

Folder 'cbioondemandK8S' holds Dockerfile and Java application 'API' which is packaged to image.

These images are stored in Gitlab image repository. Tag policy is set as \<branch\>-\<hash_of_commit\>. With this policy we can identifies where and when was application/image built.

Folder 'k8s' holds configuration for Kubernetes (deployment, service and definition of custom Object)

## Deployment/Pipelines
Everything should be templates and jobs are only special configuration of templates.

- Stages (master/beta/dev):
  - Build (auto/auto/auto)- API image ('cBioOnDemand folder'). Used in Deploy stage
  - Deploy (manual/auto/auto) - grab image of API from build stage and deploy it to Kubernetes

## Special configuration
- tolerations -> This ensure that Pod can be placed on Master node.
- nodeAffinity -> Ensure that Pod will be placed on Master node.

## WARNING!
As for now, there is no cleaning job for this images. Also there needs to be settle up some "removing images" policy. !!!
