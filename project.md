---
layout: page
title: Project Info
permalink: /project/
description: "EurOPDX DataPortal - information about the project."
nav_order: 2
---

# Project info

EurOPDX DataPortal is a part of [EurOPDX research infrastructure](https://www.europdx.eu/europdx-research-infrastructure/europdx-research-infrastructure-about)
and has been developed within [EDIReX project](https://www.europdx.eu/news/introducing-edirex). 

DataPortal's aim it to provide unified data repository for improved collaboration among cancer researchers. 

It enables the researchers to explore and filter [PDX models](https://en.wikipedia.org/wiki/Patient_derived_xenograft)
and export selected group of data to on-demand tool (like [cBioPortal](https://cbioportal.europdx.eu/cbioportal/)) where in-detail analysis
can be done.

