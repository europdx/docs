---
layout: default
title: Rest API
nav_order: 1
has_children: false
parent: DataHub-dev
grand_parent: Developer Documentation
permalink: /devdoc/datahub/api/

---

# DataHub - REST API

[Datahub REST API](https://gitlab.ics.muni.cz/europdx/datahub/datahub-docker) is implemented as python project using [Flask](https://flask.palletsprojects.com/en/1.1.x/) framework.
Using multiple URL resources allows user to retrieve data from DataPortal in form of JSON structure. 

All of the available resource are described in the [Swaggerhub](https://app.swaggerhub.com/apis-docs/RadimPesa/datahub/v1) documentation.

Currently, Datahub REST API is used primarly for the cBio-on-demand service.

<!---
- jak je API implementovano
- popis API (asi odkaz na Swagger)
- kdo a jak ted API vyuziva(Dataportal, cBioOnDemand) 
- ...

-->

