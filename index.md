---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: default
title: Home
nav_order: 1
description: "Documentation of EurOPDX DataPortal"
permalink: /
---
# EurOPDX DataPortal

Welcome to the documentation for EurOPDX [DataPortal](https://dataportal.europdx.eu/).

EurOPDX DataPortal is a part of EurOPDX research infrastructure
and has been developed within EDIReX project. More information about the project you can find in [this section](https://europdx.gitlab-pages.ics.muni.cz/docs/project/).

Below please find an index of available documentation for DataPortal's users, administrators and developers.

### Users
- [How to use DataPortal](https://europdx.gitlab-pages.ics.muni.cz/docs/userdoc/dataportal/)
- [User support for LAS platform](https://europdx.gitlab-pages.ics.muni.cz/docs/userdoc/las/)

### Adminitrators
- [DataPortal installation](https://europdx.gitlab-pages.ics.muni.cz/docs/admindoc/dataportal/install)
- [LAS - how to recover MySQL and Neo4J backup](https://europdx.gitlab-pages.ics.muni.cz/docs/admindoc/las/recover-db.html)
- [cBIoPortal on-demand installation and maintenance](https://europdx.gitlab-pages.ics.muni.cz/docs/admindoc/cbiood/)