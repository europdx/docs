---
layout: page
title: How to recover MySQL and Neo4j backup
parent: LAS
grand_parent: Admin Documentation
nav_order: 3

---

# How to recover MySQL and Neo4j backup

## 1. Enter into backup directory
> Using `cd` command go into directory with your backup.
> The selected directory must have a **mysql** and **neo4j** directory inside.

## 2. Run script
> Execute `/home/lasuser/las-docker-edirex-ansible/roles/backup/files/restore_mysql.sh` script.

## 3. That is all.
> The script will overwrite **MySQL** inside **lasmysql** container and download image for another container called **docker-neo4j-backup** connected to neo4j data volume with **recover** parameter. This container will replace current data with data from backup and restart the container. Please check script log to make sure the process was successful.

...
