# How to contribute

1. Clone repository
2. Make changes and test locally with

  ```bundle exec jekyll serve```

   `Url` and `baseurl` in `_config.yml` has to be changes to empty string before running locally.

3. Commit & Push result to repository.
