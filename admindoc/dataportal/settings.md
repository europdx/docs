---
layout: page
title: Settings
nav_order: 3
has_children: false
parent: DataPortal
grand_parent: Admin Documentation
permalink: /admindoc/dataportal/settings

---

# DataPortal settings

## Access modalities - models management

There are following Access Modalities in DataPortal:

- Transnational Access
- Transnational Access - TEST only
- Biobanked for Transnational Access
- Collaboration only

It is possible to:
- Add models to *Biobanked for Transnational Access* 
- Add models to *Transnational Access - TEST only*
- Remove models from *Transnational Access - TEST only* (i.e. adding models to *Transnational Access*)

### Adding models to Biobanked for Transnational Access
1. Edit `biobanked.txt` - add IDs of PDX models, which are biobanked. Write each ID on separate line. Commit this to the repository.
2. Select desired CI job `add_biobanked_<instance>` for deployment of the update to the `<instance>`


### Adding models Transnational Access - TEST only
1. Edit `ta_test_models_add.txt` - add IDs of PDX models, which are TA test only, write each ID on separate line. Commit this to the repository.
2. Select desired CI job `add_ta_test_<instance>` for deployment of the update to the `<instance>`


### Removal models from Transnational Access - TEST only
1. Edit `ta_test_models_remove.txt` - add IDs of PDX models, which are **not** TA test only, write each ID on separate line. Commit this to the repository. This should add _TA Acccess_ to models specified in the txt file.
2. Select desired CI job `add_ta_test_<instance>` for deployment of the update to the `<instance>`

*(This info has been taken from https://gitlab.ics.muni.cz/europdx/datahub/ta-biobanked-models-management/-/blob/master/README.md)*
