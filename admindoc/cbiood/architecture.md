---
layout: default
title: Architecture
nav_order: 1
has_children: false
parent: cBioOnDemand
grand_parent: Admin Documentation
permalink: /admindoc/cbiood/architecture

---
# cBioOnDemand Architecture

Whole cBioOnDemand subsystem consists of following main parts:

* cBioPortal instance(s)
* System components
  * Proxy
  * API


## Architecture of cBioPortal instance

Each cBioOnDemand instance consist of following Kubernetes objects:
* Two ReplicaSets(RS)
  * One RS for Cbio application with one pod included
  * One RS for MySql database with one pod included
* Two Services (cloud)
  * One Service for cbio application
  * One Service for Database RS
* One CronJob(clock)

Kubernetes yaml definitions are available at [https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/api/tree/master/cbioondemandK8S/src/main/resources](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/api/tree/master/cbioondemandK8S/src/main/resources)

Whole instance is roughly sketched here:

![](https://gitlab.ics.muni.cz/europdx/docs/raw/master/assets/cbio-app.jpg)

## Architecture of cBioPortal system components

Besides the cBioportal instances started by users, there are two main system components (API, Proxy) in the cBioPortal subsystem.

Roughly sketch here:
![](https://gitlab.ics.muni.cz/europdx/docs/raw/master/assets/architecture.jpg)
Green line shows area of control for API and black one is communication for which Proxy is responsible.

And here:
![](https://gitlab.ics.muni.cz/europdx/docs/raw/master/assets/cbio-app-proxy.jpg)

### API
API serves basic CRUD operations on user\`s instances. Used for managing cBioPortal instance lifecycle.

### Proxy
Proxy routes all incoming request to instance which match given path after domain (e.g. Domain/13123 to instance1). Entry point of network communication to Kubernetes cluster is Ingress(we use nginx). Ingress(deployed on all master nodes) routes 'Domain/' to Proxy and 'Domain/api' to API.

Detailed view of https communication of .

`Ingress -> Proxy -> Service (one for each instance) -> Pod (cBio app).`

## Kubernetes Cluster

Whole cBioOnDemand subsystem is built on top of Kubernetes cluster. Our Kubernetes Cluster
consists of 3 master nodes and variable number of worker nodes. Kubernetes cluster runs on  [MU OpenStack cloud environment](https://cloud.muni.cz).

Worker nodes have L3 connectivity with only private IPv4 addresses. All Master nodes have public IPv4 addresses and Nginx Ingress on them for external connectivity.

There are worker nodes which are dedicated for cBioOnDemand workload. We ensure this
by applying taints and labels on these nodes.

Kubernetes objects are defined at [https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/Kubernetes-docker/tree/master/yaml](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/Kubernetes-docker/tree/master/yaml)
