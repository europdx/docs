---
layout: default
title: Infrastructure
nav_order: 1
has_children: false
parent: Admin Documentation
permalink: /admindoc/infrastructure/

---

# Infrastructure

### Data Portal
Serves as the entry point to the whole infrastructure. User needs to have registered account to be able to access the data. This can be done by multiple identity providers (Google, LinkedIn or ORCID). After user logs in, he can access the PDX models and filters them by different criteria. Furthermore, user is able to send the selection of his/her choice to the cBioPortal instance.

### Data Hub

Data Hub is the middlepoint, which gathers, collects abd transfers data between the Data Portal and cBioPortal instances. Its main functionality is provided through REST API, which communicates with Data Portal and retrieves data from there. Some additional information, which are needed for proper populating of the cBioPortal, are stored inside the Data Hub image in the SQL database. 

### K8s - instances of cBioPortals
The [Kubernetes](https://kubernetes.io/) framework is used for automatic deployment of custom instances of cBioPortal. This part of infrastructure hold two API which serves for demanding data and initialization of the cBP, alongside with the already running instances.

![](https://gitlab.ics.muni.cz/europdx/docs/raw/master/images/191110-EurOPDX-Architecture.png)

<!---
- Na cem bezi DataPortal, DataHub, cBioOnDemand
- Jak pripravit stroje / virtualky, aby to mohlo bezet
(u cBoD / K8s bude asi odkaz na detailnejsi popis nekam do casti cBoD / K8s)
*mozna se to pak presune primo pod Admin Documentation
-->
