---
layout: default
title: SQL Database
nav_order: 2
has_children: false
parent: DataHub-dev
grand_parent: Developer Documentation
permalink: /devdoc/datahub/db/

---

# DataHub - SQL DB

The relational model of the DataHub's SQL DB is shown here:

![](https://gitlab.ics.muni.cz/europdx/docs/raw/master/images/DB-schema.png)

<!---
- K cemu tam je DB
- struktura DB / datovy model
- jak se v kodu pouziva (pres ten SQL Alchemy)
- ...
-->


