---
layout: default
title: CI/CD of K8S configuration
nav_order: 1
has_children: false
parent: CI/CD
grand_parent: Documentation for cBioOnDemand
permalink: /cbiooddoc/ci-cd/configuration/

---

# CI/CD configuration
[GitRepo](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/Kubernetes-docker)
# Structure
We have three branches which are identical with deployment environment.

Folder 'builds' holds Dockerfiles and required files for building images for database on cbiood and 'job' which is responsible for deleting instances.

These images are stored in Gitlab image repository. Tag policy is set as \<branch\>-\<db/job\>-\<hash_of_commit\>. With this policy we can identifies where and when was application/image built.

# !WARNING!
As for now, there is no cleaning job for this images. Also there needs to be settle up some "removing images" policy. !!!

Folder 'mellon' holds mellon configuration for each branch/environment.  

Folder 'yaml' holds Kubernetes configuration for cBiood.
  - appConfig -> configuration for cbio instances. Determine which datahub will be used to populate instance
  - configMap -> configuration for all cbio databases.
  - ingress-proxy -> configuration for ingress (external endpoint)

# Deployment/Pipelines
Everything should be templates and jobs are only special configuration of templates.

- Stages:
  - Prepare - Deploy configuration for mellon ('mellon folder' + key) to Kubernetes
  - Build - database and job ('builds folder'). Used in cBiood API
  - Deploy - deploy configuration ('yaml folder') to Kubernetes
