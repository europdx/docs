---
layout: default
title: Installation
nav_order: 1
has_children: false
parent: DataHub
grand_parent: Admin Documentation
permalink: /admindoc/datahub/install/

---


# DataHub - Installation
  DataHub needs ubuntu virtual machine (or any other linux distro) with `docker` and `docker-compose` installed. After setting up virtual evniroment, pulling [DataHub-docker](https://gitlab.ics.muni.cz/europdx/datahub/datahub-docker) repository, you should be able to start up the application wiht `docker-compose build` and `docker-compose up`. After all containers are runnig, execute
  - `docker exec -it api bash`
  - `flask db init`
  - `flask db migrate`
  - `flask db upgrade`


  This should initialize database with proper scheme designed for work with PDX data. After this, you should be able to access DataHub through `localhost:8080/api/v1/`. All other resources are described [here](https://app.swaggerhub.com/apis-docs/RadimPesa/datahub/v1#/). Further you should continue with uploading the data, which is described [here](import.md).

<!-- 
  ---------------

- co je potreba udelat pro instalaci "z ciste vody"
- ze musi byt pripravena virtualka viz [zde](/docs/admindoc/infrastructure/)
- co se musi nastavit, pustit
- co musi nasledovat (viz [Import Dat](/docs/admindoc/datahub/import/))
- ... -->
