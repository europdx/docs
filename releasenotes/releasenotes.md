---
layout: default
title: Release Notes
nav_order: 6
has_children: false
permalink: /releasenotes/

---

# EurOPDX Data Portal - Release Notes

## Release 2.1
----------
*[ 29th Jul 2021]*
- Static cBioPortal upgrade to v3.6.7 - available at [cbioportal.europdx.eu](https://cbioportal.europdx.eu/)

- Fixes in DataPortal on PDX Model Details page
  - Corrected displaying of user login
  - Details for some PDX models did not display - correcred
- Explicit information about 'Acceptable Use Policy and Conditions of Use' before downloading data
  - Modal dialog at search page
  - Text information at PDX Model Details page
- EDIReX Galaxy Pipelines with DataPortal Sandbox available at [pipeline-prod.edirex.ics.muni.cz](http://pipeline-prod.edirex.ics.muni.cz/)
  - Data provider can process acquired raw sequencing data via a bioinformatics pipeline implemented on Galaxy platform and validate the result dataset in a sandbox DataPortal and cBioPortal
  - For more information see [EDIReX Galaxy Pipelines documentation](https://europdx.gitlab-pages.ics.muni.cz/pdx-pipelines/wiki-page/)

## Release 2.0.2
----------
*[ 30th Mar 2021]*
- Merge of PDXFidner (commit from Jan 21st, 2021)
- Search according to PdxModelID moved to 'Filter By' section
- Data set from 9th Nov 2020

## Release 2.0.1
----------
*[ 27th Sep 2020]*
- More samples per patient can be exported to cBioPortal
- Searching according to PdxModelID - more ModelIDs / cancer types enabled
- IRCC-GC - gistic values export to cBioPortal - fixed
- Dosing study data are not visible in DataPortal model details - fixed


## Release 2.0
----------
*[ 6th Aug 2020 ]*
- Merge of PDXFinder 6.0
- New dataset from June 2020 available (995 PDX models, 8 research centres, 7 cancer diagnoses)
- New filter "Transnational Access - TEST only"
- Searching according to PdxModelID enabled


## Release 1.1:
----------
*[ March 2020 ]*

- IRCC-GC molecular data are available
- DataHub DB model revised / reworked - molecular data (CNAs, Mutations) are now loaded from N4J DB
- Filter for already biobanked models implemented (see filter Access Modalities / Biobanked for Transnational access)
- In section Search / Export - correction of statistics for molecular data per study


