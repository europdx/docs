---
layout: page
title: LAS
nav_order: 3
has_children: false
parent: User Documentation
permalink: /userdoc/las/

---

# User support for LAS platform within edirex project

## Testing Phase
Now we are in a **testing phase** of the LAS deployment. We are sorry for any inconvenience which can arise from LAS fine-tuning during these first months. We are doing our best to have **instances for all national nodes of the consortium** ready as soon as possible to switch them to a production version and allow you to use your LAS instance for everyday work in a laboratory.

Please, if you have any question or note to the LAS functionalities or deployment, do not hesitate to write to us at <las-edirex@ics.muni.cz>.

Thank you in advance for your cooperation.


## Tips, Questions and Answers
At following link answers for often questions can be found (work in progress).
* [Tips, questions, answers](FAQ.md)

## Remote Support
Download and launch file from the following link
*  [Remote support for Windows](https://gitlab.ics.muni.cz/europdx/support/raw/master/files/TeamViewerQS.exe)
*  [Remote support for Mac](https://gitlab.ics.muni.cz/europdx/support/raw/master/files/TeamViewerQS.dmg)

## About Us
We are a team from Masaryk University held in Brno, Czech Republic. During the period from 2018 to 2022 when EDIReX project will last, we will maintain the **LAS for EurOPDX consortium** at our servers and provide the user support at the best quality we can. This in cooperation with a team of developers from Torino, Italy.
