---
layout: page
title: Infrastructure overview
permalink: /overview/
nav_order: 3
---

# Infrastructure overview

EurOPDX Data Portal infrastructure (EuroPDX IT research infrastructure) consists of following components:
- **DataPortal / PDX search tool** - the tool for searching and exploring PDX models, enabling user to export
the selected models' data into on-demand tools
- **cBioPortal** - tool for visualisation of various cancer genomics data sets; there is one common cBioPortal instance
populated with all available data; cBioPortal instance can be also created upon user's request and can be populated with user data
- **DataHub** - the main data repository
- **LAS** - Laboratory Assistant Suite - system for collecting PDX models data from laboratories
- Genome Cruzer (in preparation)

The overall structure of EDIReX IT infrastructure:

![](https://gitlab.ics.muni.cz/europdx/docs/raw/master/images/EDIReX-structure.png)

### Additional info

- [EDIReX Project - presentation (CZ)](https://docs.google.com/presentation/d/1wxs6riEnTsbfMCJn4mgLZeJBJ3FZ72KQd7tjuZ43IyI/edit#slide=id.p)
- [EDIReX Project - presentation as .pptx (CZ)](https://gitlab.ics.muni.cz/europdx/edirex-doc/blob/master/Presentations/EDIReX-prezentace-kolokvium-191113.pptx)
