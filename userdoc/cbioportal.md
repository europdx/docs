---
layout: page
title: cBioPortal
nav_order: 2
has_children: false
parent: User Documentation
permalink: /userdoc/cbioportal/
description: "What is cBioPortal, how to use it."

---

# cBioPortal

cBioPortal is an open source tool for visualising big molecular data of the PDX models. 
It has been developed by [Memorial Sloan Kettering Cancer Center](https://www.mskcc.org/).

Within EurOPDX DataPortal there is run an own [cBioPortal instance](https://cbioportal.europdx.eu/cbioportal/) populated with only EurOPDX data collections.

In addtition, DataPortal's users are able to export a group of PDX models' data to their own instance of cBioPortal as described in 
[this section](https://europdx.gitlab-pages.ics.muni.cz/docs/userdoc/dataportal/).

How to work with cBioPortal you can fin in the [official tutorial](https://www.cbioportal.org/tutorials).
