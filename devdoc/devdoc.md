---
layout: default
title: Developer Documentation
nav_order: 6
has_children: true
permalink: /devdoc/

---

# Developer Documentation

This section covers developer documentation for DataPortal infrastructure.

### Infrastructure schema
The overall schema of the infrastructure and its components is shown below:

![](https://gitlab.ics.muni.cz/europdx/docs/raw/master/images/191110-EurOPDX-Architecture.png)
