---
layout: default
title: Data storage
nav_order: 3
has_children: false
parent: DataHub
grand_parent: Admin Documentation
permalink: /admindoc/datahub/data/

---


# DataHub - Data storage

Currently (Jun 20') most of the needed data for the populating of the custom instance of cBP are stored in the Data Portal graph database Neo4j. According the the available resources described in the [Swagger hub](https://app.swaggerhub.com/apis-docs/RadimPesa/datahub/v1#/), each resource has is equivalent implemented in [Cypher query language](https://neo4j.com/developer/cypher-query-language/) as part of the Data Hub. This query retrieves results which are later on parsed and returned as output in JSON format.

<!-- ------------
- Kde jsou ulozeny data - https://gitlab.ics.muni.cz/europdx/datahub/data-release
- Odkud se vezmou, jak / kam se nahraji
- Jakym zpusobem jsou verzovany
- ... -->
