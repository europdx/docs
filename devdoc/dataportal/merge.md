---
layout: page
title: PDX Finder Merge
nav_order: 2
has_children: false
parent: DataPortal-dev
grand_parent: Developer Documentation
permalink: /devdoc/dataportal/merge

---

# DataPortal - PDX Finder Merge
As we are not in charge of the development of PDX Finder, which is the core of the Data Portal, we have to carefully merge our custom changes into new releases of the PDX Finder to sustain new functionality. This is done by following steps:

- `git branch new-branch` creating new branch in the [dataportal-sourcecode](https://gitlab.ics.muni.cz/europdx/dataportal/dataportal-sourcecode) from the master

- `git checkout` to this new branch 

- run `git remote add <remote_name> <link_to_github_pdxfinder>`, which adds remote branch to the local repository. The *remote_name* can be anything, it serves only for you to be able to distinguish from the origin branches.

Example: `git remote add pdxfinder-v6 https://github.com/PDXFinder/pdxfinder/`

- run `git pull <remote_name> <name_of_the_commit> --squash`, which will the remote branch and also squashes commits, so your commits are not lost.

Example: `git pull pdxfinder-v6 43ed7a914e86a222dde1c363283e8e94d37fecf2 --squash`
  
- Resolve conflicts 

Recommendation: use *Atom* editor (`apt-get install atom`)

- Run `git commit` which will commit the changes up to your repository


<!---
- Jak Zmergovat aktualni verzi PDX Finderu do DataPortalu
-->

<!-- ![](/assets/dataportal-merge.png) -->
