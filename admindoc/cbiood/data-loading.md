---
layout: default
title: Data-loading (cBioPortal)
nav_order: 4
has_children: false
parent: cBioOnDemand
grand_parent: Admin Documentation
permalink: /admindoc/cbiood/data-loading/

---
# Data-loading
[GitRepo](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/data-loading)

# Who Am I?
Python 'script' for loading data to cBioPortal from DataHub. Also containing cBioPortal app.


# Structure
We have three branches which are identical with deployment environment.

Folder 'Docker' holds python 'app' (data-loading 'script'), Dockerfile and required files to build image for cBioPortal app.

These images are stored in Gitlab image repository. Tag policy is set as \<branch\>-\<app\>-\<hash_of_commit\>. With this policy we can identifies where and when was application/image built.

# !WARNING!
As for now, there is no cleaning job for this images. Also there needs to be settle up some "removing images policy". !!!

# Deployment/Pipelines
Everything should be templates and jobs are only special configuration of templates.

- Stages (master/beta/dev):
  - Build (manual) - Builds cBioPortal app image which is than used in cBiood API.

- Deployment:
  Need to write (manualy) new image from build to API [resources](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/api/tree/master/cbioondemandK8S/src/main/resources) and deploy API.
