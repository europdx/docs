---
layout: default
title: Installation
nav_order: 2
has_children: false
parent: cBioOnDemand
grand_parent: Admin Documentation
permalink: /admindoc/cbiood/installation
---
# cBioOnDemand Subsystem Installation


## Openstack project
All the resources are available as a single OpenStack project. This project is managed by set of Terraform repositories.
TODO

We use [Openstack](https://dashboard.cloud.muni.cz/project/) for IaaS and we provision this infrastructure by terraform.
Terraform configuration:
 - [Rancher](https://gitlab.ics.muni.cz/europdx/k8s/rancher-tf-ansible/tree/master/tf)
 - [Cluster](https://gitlab.ics.muni.cz/europdx/k8s/k8s-tf-ansible/tree/master/tf)

 1. First step is to Install Rancher. See bellow!

## Rancher
[GitRepo](https://gitlab.ics.muni.cz/europdx/k8s/rancher-tf-ansible)
Run terraform.
Use B option.
![](https://gitlab.ics.muni.cz/europdx/docs/raw/master/assets/rancher-install.png)

You may need to add users manualy. See [doc](https://rancher.com/docs/rancher/v2.x/en/cluster-admin/cluster-members/)

## Ansible
[GitRepo](https://gitlab.ics.muni.cz/europdx/k8s/k8s-tf-ansible)
2. step is to run terraform and then ansible which dynamicly look up VMs created in
Openstack and install cluster on them. After successful Kubernetes cluster build follow
post-install steps.

!MANUAL! Need to extract token from rancher for new cluster and write it to ansible!

![](https://gitlab.ics.muni.cz/europdx/docs/raw/master/assets/cluster-install.png)
## cBioOnDemand

3. step is to install cBioOnDemand konfiguration which is located in [Kubernetes-docker](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/Kubernetes-docker) repository. You should edit readme and run whole pipeline.
Repeat for master/beta/dev.

4. step is to install certificates !TODO!
  Ingress takes certificates from secret names 'ingress'. This certificates are stored in #TODO.
  To create secret ingress run

  `kubectl create secret tls ingress --key ${KEY_FILE} --cert ${CERT_FILE}`

  For *${CERT_FILE}* file with full certification chain should be used.
  Secret *cbio-static* is used for http://cbioportal.europdx.eu instance.

5. step is to install system components from [API](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/api) and [secure-routing](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/secure-routing) repositories. You should edit readme and run whole pipeline.
Repeat for master/beta/dev.

6. DONE!
