---
layout: page
title: DataHub-dev
nav_order: 2
has_children: true
parent: Developer Documentation
permalink: /devdoc/datahub/

---

# Data Hub

The complete Data Hub application stack consists of three modules - __App__, __MySQL Database__, __Proxy__. Fourth module, which is not part of this stack is Data Portal's __neo4j Graph database__ from which Data Hub app takes data as well. Reason for this duality of databases is that cBioPortal is interested in different data then Data Portal search engine. Extra data for cBP are stored in the SQL databases and we retrieve the maximum possible amount of data from the neo4j database. 

Whole stack of these three components is dockerized and you are able to deployed at once.


### Application file structure
```bash
- api
    | - app
        | - routes.py
        | - tools.py
    | - models
        | - models.py
    | - data
    | - api.py
    | - config.py
    | - import_expr.py
    | - init_db.py
    | - proxy
```
Some files are ommited, as they are not crucial for understanding the app.

- __api/app/routes.py__
    - All available routes are described and implemented here.
- __api/app/tools.py__
    - Some of retrieval methods, used by different routes are implemented here.
- __api/models/models.py__
    - Description of all entities for the SQL databse.
- __api/api.py__
    - Main run file of the flask framework.
- __api/config.py__
    - Holds variables about connection to the databases. Needs to be edited for the _debug_ purposes. 
- __api/import_expr.py__
    - Import expressions into the database, as the DataPortal database does not holds the expression data.
- __api/init_db.py__
    - Prepares the initial empty SQL database.
- __api/proxy__
    - Content of the proxy.



<!---
- celkova architektura - Proxy - API - DB \(+ napojeni na N4J\) 
-->
