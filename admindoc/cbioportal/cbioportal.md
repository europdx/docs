---
layout: default
title: cBioPortal (static)
nav_order: 3
has_children: false
parent: Admin Documentation
permalink: /admindoc/cbioportal/

---

# "Static" cBioPortal

EuroPDX cBioPortal is hosted at [https://cbioportal.europdx.eu](https://cbioportal.europdx.eu). It is available to any user without any authentication.

## Implementation
EuroPDX cBioPortal is implemented as a cBioOnDemand instance. 

Source files are available at 
[https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/staticcbio](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/staticcbio).
Deploy steps are described in the [README.md](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/staticcbio/-/blob/master/README.md) file. 

Static TMPlist has to be available in Datahub before deploying the cBioPortal. 

## Deployment steps

[https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/staticcbio/-/blob/master/README.md](https://gitlab.ics.muni.cz/europdx/k8s/cbio-on-demand/staticcbio/-/blob/master/README.md)

## SSL certificate for static cBioPortal

For *${CERT_FILE}* file with full certification chain should be used.

  `kubectl create secret tls cbio-static -n cbio-on-demand --key ${KEY_FILE} --cert ${CERT_FILE}`

## Static cBioPortal SSL certificate renewal

1. Renew TLS certificate
2. Recreate tls secret:

    `kubectl delete secret cbio-static -n cbio-on-demand`

     `kubectl create secret tls cbio-static -n cbio-on-demand --key privkey.pem --cert fullchain.pem`
