---
layout: page
title: Installation
nav_order: 1
has_children: false
parent: DataPortal
grand_parent: Admin Documentation
permalink: /admindoc/dataportal/install

---

# DataPortal - installation

DataPortal installation is automated using GitLab CI/CD but only for the following domains.
``dataportal.edirex.ics.muni.cz`` (alias for ``dataportal.europdx.eu``)  
``dataportal-beta.edirex.ics.muni.cz``  
``dataportal-dev.edirex.ics.muni.cz``

For more info about the installation backround, see [Developer Documentation > Installation](/devdoc/dataportal/install).

## 1. Create a blank server

> [Admin Documentation > Infrastructure](/admindoc/infrastructure/)

## 2. Prepare the server and install necessary software equipment using ansible

### 2.1. Run new pipeline in GitLab.  
> Open **DataPortal-ansible repo > CI/CD > Pipelines > [Run Pipeline](https://gitlab.ics.muni.cz/europdx/dataportal/dataportal-docker-edirex-ansible/pipelines/new)** and click to **Run Pipeline**. No environment variables are needed.
>
> ![](/assets/start-dataportal-0.png)

### 2.2. Second stage of the pipeline

> **Wait for first stage of the pipeline to finish** and **start manual job** in the deploy stage to deploy selected DataPortal.
>
> ![](/assets/start-dataportal-1.png)

## 3. Create containers and start the server

> Open **DataPortal-docker repo > CI/CD > Pipelines > [Run Pipeline](https://gitlab.ics.muni.cz/europdx/dataportal/dataportal-docker/pipelines/new)**.  
**Be sure to select right branch** and click to **Run Pipeline**. No environment variables are needed.
>
> ![](/assets/start-dataportal-2.png)